@extends('layouts.app')

@section('content')

    <table class="table table-stripped">
        <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Message</th>
        <th>created_at</th>

        </thead>
        <tbody>
        @foreach($messages as $message)
            <tr>
                <td>{{ $message->id }}</td>
                <td>{{ $message->name }}</td>
                <td>{{ $message->email }}</td>
                <td>{{ $message->message }}</td>
                <td>{{ $message->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
