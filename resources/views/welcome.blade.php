@extends('layouts.app')

@section('content')

        <div class="container">
            <div class="row">
                <div class="col-md-6 left-side">
                    <h1 class="title">Contact Us</h1>
                    <img src="{{ asset('/images/photo_1.svg')}}" alt="">
                </div>
                <div class="col-md-6 right-side">
                    <form action="{{ route('store') }}" method="post">
                        @csrf
                        <div class="form">
                            @if(session('success'))
                                <div class="alert alert-success mt-5 form-control">{{ session('success') }}</div>
                            @endif

                            @error('name') <span class="alert text-danger">{{ $message }}</span> @enderror
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                            @error('email') <span class="alert text-danger">{{ $message }}</span> @enderror
                            <input type="text" class="form-control" name="email" placeholder="Mail" value="{{ old('email') }}"  autocomplete="email" autofocus>

                            @error('message') <span class="alert text-danger">{{ $message }}</span> @enderror
                            <textarea  class="form-control message" name="message" placeholder="Message" value="{{ old('message') }}"  autocomplete="message" autofocus></textarea>

                            <button class="button" type="submit">Submit</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection
